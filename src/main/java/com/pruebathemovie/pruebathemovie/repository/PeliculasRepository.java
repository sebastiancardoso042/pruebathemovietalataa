package com.pruebathemovie.pruebathemovie.repository;

import com.pruebathemovie.pruebathemovie.dto.PeliculasDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PeliculasRepository extends JpaRepository<PeliculasDTO,String> {
}
