package com.pruebathemovie.pruebathemovie.dto;

import com.sun.net.httpserver.Authenticator;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.lang.NonNull;

@Data
//@NoArgsConstructor
@AllArgsConstructor
public class ListaPeliculas {

    private String names;

    private Integer value;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
